import { useContext } from "react";
import { ThemeContext } from "../context/Provider";
import { createGlobalStyle } from "styled-components";

const StyledGlobalStyle = createGlobalStyle`
    * {
        font-family: 'Trebuchet MS', Helvetica, sans-serif;
        padding: 0;
        margin: 0;  
        box-sizing: border-box;
    }
 
    body {
         ${
				"" /* Ici cette syntaxe revient au même que
        background-color: ${({ props }) => props.isDarkMode ? "#2F2E41" : "white"};     */
			}
        background-color: ${({ isDarkMode }) =>
			isDarkMode ? "black" : "white"};
        color: ${({ isDarkMode }) => (isDarkMode ? "white" : "black")};
    }
`;

function GlobalStyle() {
	const { theme } = useContext(ThemeContext);

	return <StyledGlobalStyle isDarkMode={theme === "dark"} />;
}

export default GlobalStyle;
