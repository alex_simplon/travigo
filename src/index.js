import "./App.css";
import React from "react";
import ReactDOM from "react-dom/client";
// npm i react-router-dom
import { BrowserRouter as Router } from "react-router-dom";
// npm i react-router
import { Routes, Route as ReactRoute } from "react-router";

import "./index.css";
// import App from "./App";
import Header from "./components/Header";
// import Carte from "./components/Carte";
import Footer from "./components/Footer";
import NotFound from "./components/NotFound";
import Home from "./routes/Home";
import Propos from "./routes/Propos";
import Contact from "./routes/Contact";
import Voyages from "./routes/Voyages";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    {/* <App /> */}
    <Router>
      <Header />
      <Routes>
        <ReactRoute path="/" element={<Home />}></ReactRoute>

        <ReactRoute path="/Propos/" element={<Propos />}></ReactRoute>

        <ReactRoute path="/Voyages/" element={<Voyages />}></ReactRoute>

        <ReactRoute path="/Contact/" element={<Contact />}></ReactRoute>

        <ReactRoute path="*" element={<NotFound />}></ReactRoute>
      </Routes>
      <Footer />
    </Router>
  </React.StrictMode>
);
