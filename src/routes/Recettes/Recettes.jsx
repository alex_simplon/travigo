import { Loader } from "../../utils/style/Atoms";
import styled from "styled-components";
import { useState, useEffect } from "react";

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

function Recettes() {
  const [searchValue, setSearchValue] = useState("");

  const handleChange = (event) => {
    setSearchValue(event.target.value);
  };

  const fetchData = async () => {
    // const response = await fetch(
    //   `https://www.themealdb.com/api/json/v1/1/search.php?s=${searchValue}`
    // );
    // if (!response.ok) {
    //   throw new Error("Data could not be fetched!");
    // } else {
    //   return response.json();
    // }

    try {
      const response = await fetchfetch('https://apidojo-booking-v1.p.rapidapi.com/currency/get-exchange-rates?base_currency=USD&languagecode=en-us', options);
      return response.json();
    } catch (err) {
      throw new Error(err);
    }
  };

  useEffect(() => {
    fetchData()
      .then((response) => {
        console.log(response);
      })
      .catch((e) => {
        console.log(e.message);
      });

    console.log(searchValue);
  }, [searchValue]);

  return (
    <div>
      <h2>Trouvez votre Voyages culinaire ! </h2>
      <form>
        <input
          type="text"
          value={searchValue}
          onChange={handleChange}
          placeholder="Rechercher un voyage culinaire..."
        />
      </form>
      <LoaderWrapper>
        <Loader />
      </LoaderWrapper>
    </div>
  );
}

export default Recettes;


