import React, { useState, useEffect } from "react";
import "../../App.css";
import styled from "styled-components";
import Carte from "../../components/Carte";


const GridContainer = styled.div`
  display: grid;
	grid-template-columns: repeat(3, 1fr);
	grid-template-rows: repeat(auto, 1fr);
	margin: 5% 5% 20% 5%;
	gap: 5%;
	text-align: center;
  z-index: -100;
 `;

function Voyages() {
  const [voyages, setVoyages] = useState([]);

  useEffect(() => {
    fetch("data.json")
      .then((response) => response.json())
      .then((data) => setVoyages(data.voyages))
      .catch((error) => console.error(error));
  }, []);


  return (
    // <div className="container_carte">
    <GridContainer>
      {voyages.map((voyage) => (
        <Carte key={voyage.id} images={voyage.images} pays={voyage.pays} date={voyage.date} duree={voyage.duree} description={voyage.description} prix={voyage.prix}/>        
      ))}
    </GridContainer>
  );
}

export default Voyages;