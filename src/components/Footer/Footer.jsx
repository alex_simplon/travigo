import styled from "styled-components";

// Styles sur le 'footer container'
const FooterContainer = styled.footer`
	height: 60px;
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: space-around;
	background-color: #e4bf94;
	box-shadow: 0px 3px 8px #000000;
	z-index:100;
`;

// Styles de paragrahes dans le footer
const Paragraphe = styled.p`
	color: #00000060;
	letter-spacing: .5px;
`;

// Effect :hover extend de 'const Paragraphe'
const ParaHover = styled(Paragraphe)`
	&:hover {
		color: #000000;
		scale: 1.05;
	}
`;

// Retourne le contenu
function Footer() {
	return (
		<FooterContainer>
			{/* Ceci est la section footer */}
			<Paragraphe>
				<span>&copy;</span>2023
			</Paragraphe>
			<Paragraphe>travigo | Alexandre & Christian</Paragraphe>
			<Paragraphe>France - Grenoble</Paragraphe>
			<ParaHover>mentions légales</ParaHover>
		</FooterContainer>
	);
}

export default Footer;
