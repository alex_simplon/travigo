import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
	padding: 15px;
	color: #8186a0;
	text-decoration: none;
	font-size: 18px;

	&:hover {
		color: #000;
		border-radius: 3px;
		background-color: #8186a020;
		box-shadow: 0px 0px 4px #00000070;
	}
`;

const NavContainer = styled.nav`
	padding: 30px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	background-color: #fff;
	box-shadow: 0px 3px 8px #00000040;
`;

function Header() {
	return (
		<NavContainer>
			<div>
				<StyledLink to="/">Accueil</StyledLink>
				<StyledLink to="/propos">A propos</StyledLink>
				<StyledLink to="/voyages">Nos Voyages</StyledLink>
				<StyledLink to="/contact">Contact</StyledLink>
			</div>
		</NavContainer>
	);
}

export default Header;
