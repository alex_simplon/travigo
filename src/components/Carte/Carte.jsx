import styled from "styled-components";
// import Voyages from "../../routes/Voyages";
import "../../App.css";

// Styles sur le 'footer container'
const CarteContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
	align-items: center;
	height: auto;
	width: 400px;
	padding: 3%;
	background-color: #f8f8f8;
	border-radius: 14px;
	box-shadow: 0 0 6px black;
`;

// Styles de paragrahes dans le footer
const Paragraphe = styled.p`
	color: #000000;
	padding: 5px;
	margin: 12px 0;
	letter-spacing: .5px;
	line-height: 1.4;
	text-align: justify;
`;
// 
// const Pics = styled.div`
// 		width: 50px;
// 		height:3.125rem;
// `;

// Retourne le contenu
function Carte(props) {
	return (
		<CarteContainer>


			<div key={props.id}>
				{/* <div> */}
					<img src={props.images} alt="capital du pays" className="pics" />
				{/* </div> */}
				<h2>Pays : {props.pays}</h2>
				<h4>Date : {props.date}</h4>
				<h4>Durée : {props.duree}</h4>
				<Paragraphe>
					<span>Description : </span>{props.description}
					</Paragraphe>

					<Paragraphe>
					<span>Prix : </span> : {props.prix} €
				</Paragraphe>
			</div>

			{/* <div key={props.id}>
				<h2>Pays : {props.pays}</h2>
				<h4>Date : {props.date}</h4>
				<Paragraphe>
					<p>Description : {props.description}</p>
					<p>Prix : {props.prix} €</p>
				</Paragraphe>
			</div> */}
		</CarteContainer>
	);
}

export default Carte;